# Testing colors

UI.new.show(
	UI::Stack.new.append(
		UI::Text.new("Simple text"),
		UI::Text.new("Red text", color: :red),
		UI::Text.new("Blue text", color: :blue),
		UI::Text.new("Green text", color: :green),
		UI::Text.new("Cyan text", color: :cyan),
	)
);
