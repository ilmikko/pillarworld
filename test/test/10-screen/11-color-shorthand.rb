# Color shorthand

# Normal \n puts doesn't work in raw mode
def puts(s)
	printf("%s\r\n",s);
end

puts("This text should be red: "+Screen::Color.text('Red text',:red));
puts("This text should be blue: "+Screen::Color.text('Blue text',0,0,255));
puts("This text should be magenta: "+Screen::Color.text('Magenta text',255,0,255));
puts("This text should be yellow: "+Screen::Color.text('Yellow text',:yellow));
puts("This text should be green: "+Screen::Color.text('Green text',:green));
