UI.new.show(
	UI::Split.new.append(
		UI::Pass.new,
		UI::Align.new.append(
			UI::Stack.new.append(
				UI::Text.new("Hello,"),
				UI::Text.new("World!"),
				UI::Text.new,
				UI::Text.new("What a beautiful"),
				UI::Text.new("day it is!"),
			)
		),
	)
);
