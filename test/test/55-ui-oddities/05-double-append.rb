#
# Make sure double appending doesn't explode the universe
#

item=UI::Text.new("Some text here.");

split=UI::Split.new;

UI.new.show(
	split.append(
		UI::Stack.new.append(
			item
		)
	)
);

sleep 1;

split.append(
	UI::Stack.new.append(
		item
	)
);

sleep 1;

split.append(
	UI::Stack.new.append(
		item
	)
);
