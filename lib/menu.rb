#
# Menu handler. Makes sure the focus is only on one element.
# Standard menu functionality.
#

require 'input';
require 'console';

class Menu
	@@input=Input.new;

	attr_accessor :element;

	def focus
		# TODO
		self;
	end

	def item_previous
		# Focus the previous item
		# TODO: if focus is already at top, try to focus parent's previous element.
		return if @focus_index==0;
		@items[@focus_index].active=false;
		@focus_index-=1;
		@items[@focus_index].active=true;
	end

	def item_next
		# TODO: Look at item_previous
		return if @focus_index>=@items.length-1;
		@items[@focus_index].active=false;
		@focus_index+=1;
		@items[@focus_index].active=true;
	end

	def append(*items)
		# Add line breaks in between and convert to elements
		elements=[];
		items.each_with_index{ |item,i|
			arr = [item.element];

			arr = [UI::Text.new]+arr if !elements.empty?;

			elements += arr;
		};

		elements = [UI::Text.new]+elements if !@items.empty?;
		
		# Append items to menu items
		@items += items;

		# Focus the menu item
		@items[@focus_index].active=true;

		# Append to the UI element
		@element.append(*elements);
		self;
	end

	def show
		@ui.show(
			UI::Align.new.append(
				@element
			)
		);
		self;
	end

	#######
	private
	#######
	
	def initialize(ui: UI.new)
		@ui=ui;
		@element=UI::Stack.new;

		@items=[];
		@focus_index=0;

		@@input.rules({
			"\e[A":->{
				# go up one
				# TODO: Not always 'up', sometimes 'previous' is not 'up'
				item_previous;
			},
			"\e[B":->{
				item_next;
			}
		});
	end
end

require 'menu/item';
