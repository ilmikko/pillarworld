# 
# UINode
# Contains public functions every element should have.
# Every UI element inherits UINode.
#

class UI::Node
	attr_accessor :id;
	attr_reader :grow;

	# TODO: Rename this to 'changed' or alternatively make it an event.
	def change;end # Hook for when something has changed

	def initialize(id: nil, width: nil, height: nil, grow: 1, **modifiers)
		@id=id;
		@xy=[0,0];

		# Size and resizing
		@wh=[0,0];
		@width_min=@width_max=@height_min=@height_max=nil;
		@content_width=@content_height=0;

		# Experimental
		@grow=grow;

		#$console.log("Set p_wh to #{[width,height]}");
		self.width=width if !width.nil?;
		self.height=height if !height.nil?;

		@state=Screen::State.new(**modifiers) if modifiers;
	end
end

require('ui/node/change');
require('ui/node/state');
require('ui/node/resizing');
require('ui/node/drawing');
require('ui/node/hierarchy');
require('ui/node/positioning');
