#
# Input library, handles input of all kinds (stdin, keyboard...).
#

require 'io/console';
require 'console';

class Input
	@@inputs=[];
	def self.close_all
		@@inputs.each{|input| input.close; };
	end

	######
	public
	######
	
	def open
		@stream.echo = false;
		@stream.raw!;

		thread;
	end

	def close
		kill_thread;

		@stream.echo = @echo;
		@stream.cooked!;
	end

	def rule(key, func)
		raise "Second argument needs to be a proc!" if !func.is_a? Proc;
		@rules[key.to_sym]=func;
	end

	def rules(enum)
		enum.each{
			|key, func| rule(key, func);
		}
	end

	# A wild key has been pressed!
	def press(key)
		$console.log("PRESS: #{key}");
		key=key.to_sym;
		if @rules.key? key
			# There is a rule for this key
			@rules[key].();
		end
	end
	
	#######
	private
	#######
	
	def thread
		@thread=Thread.new{
			while true
				char = @stream.getch;

				# Escape character
				if (char=="\e")
					# TODO: Detect just ESC by having a timeout for this next line
					char+=@stream.getch+@stream.getch;
				end

				begin
					press char;
				rescue StandardError => e
					# This is to keep the input thread from dying in case a listener has an error.
					$console.error("INPUT: ERROR: #{e}");
					$console.error("#{e.backtrace}");
				end
			end
		}
	end

	def kill_thread
		@thread.kill;
	end
	
	def initialize(stream: STDIN)
		@echo=stream.echo?;
		@stream=stream;

		# CTRL-C exits
		@rules={
			"\u0003":->{ exit; }
		};

		@@inputs << self;

		open;
	end
end

at_exit{
	# Close all input streams gracefully.
	Input.close_all;
}
