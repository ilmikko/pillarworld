#
# Menu item component
#

class Menu::Item
	attr_accessor :element;

	def active;@active;end
	def active=(v);
		# Change element color
		@active=v;
		if @active
			@element.color=:green;
		else
			@element.color=nil;
		end
		@element.redraw;
	end

	#######
	private
	#######

	def initialize(text, active: false)
		@element=UI::Text.new(text);
		self.active=active;
	end
end
